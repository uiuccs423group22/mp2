obj-m += mp2_module.o
all: test_app
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
	rm mp2_test_app
test_app: mp2_test_app.c
	gcc mp2_test_app.c -o mp2_test_app
