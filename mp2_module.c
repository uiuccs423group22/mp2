#include <asm/uaccess.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/list.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/timer.h>

#include "mp2_module.h"
#include "mp2_given.h"

MODULE_DESCRIPTION("MP2 Module");
MODULE_AUTHOR("Andrew Louis, Timothy Madigan, Laura Makdah");
MODULE_LICENSE("GPL");

// Declare some global variables.
static struct kmem_cache * mem_cache;
static struct proc_dir_entry * mp2_proc_dir;
static struct proc_dir_entry * mp2_proc_status_file;
static mp2_task_struct_t task_list;
static spinlock_t task_list_lock;
static unsigned long lock_flags;
static mp2_task_struct_t * mp2_curr_task;
static struct task_struct * dispatch_thread;
static long current_load = 0;

/*
 * A boolean function that determines whether or not adding a task
 * with the given period and computation duration will put the scheduler
 * at risk of being unable to satisfy deadlines.
 */
static char canBeAdmitted(long period, long computation)
{
	long ratio = computation*1000/period;
	return current_load + ratio <= 693;
}

/*
 * Finds the first occurrence of token in src and replaces it with
 * a null character.  Returns the address immediately after the 
 * first occurrence of token.  Useful for parsing proc input.
 */
static char * stringTokenizer(char * src, char token)
{
	char * curr = src;
	while(*curr != token && *curr != '\0')
	{
		curr++;
	}
	*curr = '\0';
	return curr+1;
}

/*
 * Worker function for the kernel thread devoted to ensuring that the
 * task that has work to do and has the highest priority is scheduled.
 * Also ensures that all other processes are not scheduled.
 */
static int dispatch_thread_func(void * data)
{
	while(!kthread_should_stop()) // Keep running until exit module sends stop message
	{
		mp2_task_struct_t * highest_priority = NULL;
		mp2_task_struct_t * pos;
		spin_lock_irqsave(&task_list_lock, lock_flags);
		list_for_each_entry(pos, &task_list.list, list) { // find highest priority task
			if(pos == NULL)
			{
				printk("Dispatch thread pos is null.\n");
			}
			else if(pos->state != SLEEPING_STATE)
			{
				if(highest_priority == NULL)
				{
					highest_priority = pos;
				}
				else
				{
					if(pos->period < highest_priority->period)
					{
						highest_priority = pos;
					}
				}
			}
		}
		spin_unlock_irqrestore(&task_list_lock, lock_flags);
		if(highest_priority != NULL) // schedule highest priority task (if any)
		{
			if(mp2_curr_task != highest_priority)
			{
				struct sched_param sparam;
				if(mp2_curr_task != NULL)
				{
					mp2_curr_task->state = READY_STATE;
					sparam.sched_priority = 0;
					sched_setscheduler(mp2_curr_task->linux_task, SCHED_NORMAL, &sparam);
				}
				highest_priority->state = RUNNING_STATE;
				wake_up_process(highest_priority->linux_task);
				sparam.sched_priority = MAX_USER_RT_PRIO-1;
				sched_setscheduler(highest_priority->linux_task, SCHED_FIFO, &sparam);
				mp2_curr_task = highest_priority;
			}
		}
		else
		{
			mp2_curr_task = NULL;
		}
		set_current_state(TASK_UNINTERRUPTIBLE);
		schedule();
	}
	return 0;
}

/*
 * Triggered at the start of a task's period.
 * Resets the timer and wakes up the dispatch thread because
 * rescheduling may be needed.
 */
static void timer_callback(unsigned long pid)
{
	mp2_task_struct_t * pos;
	spin_lock_irqsave(&task_list_lock, lock_flags);
	list_for_each_entry(pos, &task_list.list, list) {
		if(pos->linux_task->pid == pid)
		{
			pos->state = READY_STATE;
			mod_timer(&pos->wakeup_timer, jiffies + msecs_to_jiffies(pos->period));
		}
	}
	spin_unlock_irqrestore(&task_list_lock, lock_flags);
	wake_up_process(dispatch_thread);
}

/*
 * Handles registration of a task with the given process id (pid),
 * period, and computation duration.  If the task passes admission
 * control, its data structures are initialized and it is added to
 * the list of registered tasks.
 */
static void registrationHandler(int pid, long period, long computation)
{
	printk("Received registration: %d, %ld, %ld\n", pid, period, computation);
	if(canBeAdmitted(period, computation))
	{
		mp2_task_struct_t * newEntry = kmem_cache_alloc(mem_cache, GFP_ATOMIC);
		newEntry->period = period;
		newEntry->computation = computation;
		current_load += computation*1000/period;
		newEntry->state = SLEEPING_STATE;
		newEntry->linux_task = find_task_by_pid(pid);
		newEntry->hasDoneFirstYield = 0;
		setup_timer(&newEntry->wakeup_timer, timer_callback, pid);
		INIT_LIST_HEAD(&newEntry->list);
		spin_lock_irqsave(&task_list_lock, lock_flags);
		list_add(&newEntry->list, &task_list.list);
		spin_unlock_irqrestore(&task_list_lock, lock_flags);
	}
	else
	{
		printk("Process %d denied admission - load exceeded.\n", pid);
	}
}

/*
 * Handles state changes when the task with the given process id (pid) yields.
 * Wakes up the dispatch thread to handle any necessary rescheduling.
 */
static void yieldHandler(int pid)
{
	printk("Received yield: %d\n", pid);
	mp2_task_struct_t * pos;
	spin_lock_irqsave(&task_list_lock, lock_flags);
	list_for_each_entry(pos, &task_list.list, list) {
		if(pos->linux_task->pid == pid)
		{
			if(pos->hasDoneFirstYield)
			{
				pos->state = SLEEPING_STATE;
				set_task_state(pos->linux_task, TASK_UNINTERRUPTIBLE);
			}
			else
			{
				pos->state = READY_STATE;
				mod_timer(&pos->wakeup_timer, jiffies + msecs_to_jiffies(pos->period));
				pos->hasDoneFirstYield = 1;
			}
		}
	}
	spin_unlock_irqrestore(&task_list_lock, lock_flags);
	wake_up_process(dispatch_thread);
}

/*
 * Handles deregistration of the task with the given process id (pid).
 * Deletes all data structures associated with the task.
 */
static void deregistrationHandler(int pid)
{
	printk("Received deregistration: %d\n", pid);
	if(pid == mp2_curr_task->linux_task->pid)
	{
		mp2_curr_task = NULL;
	}
	struct list_head * pos, * q;
	mp2_task_struct_t * entry;
	spin_lock_irqsave(&task_list_lock, lock_flags);
	list_for_each_safe(pos, q, &task_list.list) {
		entry = list_entry(pos, mp2_task_struct_t, list);
		if(entry->linux_task == NULL)
		{
			printk("WHAT THE ****.\n");
		}
		if(pid == entry->linux_task->pid)
		{
			current_load -= entry->computation*1000/entry->period;
			del_timer(&entry->wakeup_timer);
			list_del(pos);
			kmem_cache_free(mem_cache, entry);
		}
	}
	spin_unlock_irqrestore(&task_list_lock, lock_flags);
	printk("Deregistered: %d\n", pid);
}

/*
 * Call back function for when user reads from /proc/mp2/status.
 * Writes a list of process ids of tasks registered for rate-monotonic scheduling.
 */
static int proc_read_callback(char * page, char ** start, off_t off, int count, int * eof, void * data)
{
	int len = 0;
	mp2_task_struct_t * pos;
	spin_lock_irqsave(&task_list_lock, lock_flags);
	list_for_each_entry(pos, &task_list.list, list) {
		if(pos->linux_task == NULL)
		{
			printk("task is null.\n");
		}
		len += sprintf(page+len, "%d\n", pos->linux_task->pid);
	}
	spin_unlock_irqrestore(&task_list_lock, lock_flags);
	*eof = 1;
	return len;
}

/*
 * Call back function for when user writes to /proc/mp2/status.
 * Uses stringTokenizer to parse input.  Passes parsed input on
 * to handlers.
 */
static int proc_write_callback(struct file *file, const char __user *buffer, unsigned long count, void * data)
{
	char * data_read = kmalloc(count * sizeof(char), GFP_ATOMIC);
	if(copy_from_user(data_read, buffer, count))
	{
		return -EFAULT;
	}
	char * pidStr = stringTokenizer(stringTokenizer(data_read, ','), ' ');
	char * garbage;
	if(data_read[0] == 'R')
	{
		char * periodStr = stringTokenizer(stringTokenizer(pidStr, ','), ' ');
		char * compStr = stringTokenizer(stringTokenizer(periodStr, ','), ' ');
		int pidR = simple_strtol(pidStr, &garbage, 10);
		long period = simple_strtol(periodStr, &garbage, 10);
		long comp = simple_strtol(compStr, &garbage, 10);
		registrationHandler(pidR, period, comp);
	}
	else if(data_read[0] == 'Y')
	{
		int pidY = simple_strtol(pidStr, &garbage, 10);
		yieldHandler(pidY);
	}
	else if(data_read[0] == 'D')
	{
		int pidD = simple_strtol(pidStr, &garbage, 10);
		deregistrationHandler(pidD);
	}
	kfree(data_read);
	return count;
}

/*
 * Initializes the MP2 module.
 * Each step is described.
 */
static int init_mp2_module(void)
{
	// Create proc files/directories
	mp2_proc_dir = proc_mkdir(MP2_PROC_DIR_NAME, NULL);
	mp2_proc_status_file = create_proc_entry(MP2_PROC_FILE_NAME, 0666, mp2_proc_dir);
	
	// Initialize kernel linked list of tasks
	INIT_LIST_HEAD(&task_list.list);
	
	// Create spinlock for task list
	spin_lock_init(&task_list_lock);
	
	// Create memory cache
	mem_cache = kmem_cache_create("mp2 mem cache", sizeof(mp2_task_struct_t), 0, SLAB_HWCACHE_ALIGN, NULL);
	
	// Create dispatch thread
	dispatch_thread = kthread_create(dispatch_thread_func, NULL, "mp2 dispatch thread");
	
	// Register proc callbacks
	mp2_proc_status_file->read_proc = proc_read_callback;
	mp2_proc_status_file->write_proc = proc_write_callback;
	
	return 0;
}

/*
 * Destroys the MP2 module.
 * Each step is described.
 */
static void exit_mp2_module(void)
{
	// Destroy proc files/directories
	remove_proc_entry(MP2_PROC_FILE_NAME, mp2_proc_dir);
	remove_proc_entry(MP2_PROC_DIR_NAME, NULL);
	
	// Destroy dispatch thread
	kthread_stop(dispatch_thread);
	
	// Destory List
	struct list_head * pos, * q;
	mp2_task_struct_t * entry;
	list_for_each_safe(pos, q, &task_list.list) {
		entry = list_entry(pos, mp2_task_struct_t, list);
		del_timer(&entry->wakeup_timer);
		list_del(pos);
		kmem_cache_free(mem_cache, entry);
	}
	
	// Destroy memory cache
	kmem_cache_destroy(mem_cache);
}

// Register init and exit functions
module_init(init_mp2_module);
module_exit(exit_mp2_module);
