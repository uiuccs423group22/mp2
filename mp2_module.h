/*
 * Define some constants
 */
const char * MP2_PROC_DIR_NAME = "mp2";
const char * MP2_PROC_FILE_NAME = "status";
const int SLEEPING_STATE = 0;
const int READY_STATE = 1;
const int RUNNING_STATE = 2;

/*
 * Custom linked list of processes registered for rate-monotonic scheduling.
 *
 * linux_task			- task struct used by the Linux scheduler
 * wakeup_timer			- timer used to mark a task as ready at the start of a new period
 * period				- period of computation (how often it occurs) (in milliseconds)
 * computation			- duration of computation (how long it takes) (in milliseconds)
 * state				- current state of the task (either SLEEPING_STATE, READY_STATE, or RUNNING_STATE)
 * hasDoneFirstYield	- whether or not this task has finished its first execution of yield (boolean)
 * list					- Linux kernel linked list
 */
typedef struct mp2_task_struct
{
	struct task_struct * linux_task;
	struct timer_list wakeup_timer;
	long period;
	long computation;
	int state;
	char hasDoneFirstYield;
	struct list_head list;
} mp2_task_struct_t;
