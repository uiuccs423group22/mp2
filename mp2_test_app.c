#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int PID;
long PERIOD;
long COMPUTATION = 3000;
int JOBS;

/*
 * Define factorial computiation for busy work.
 */
long factorial(int n)
{
	if(n < 0)
	{
		return -1;
	}
	if(n == 0 || n == 1)
	{
		return 1;
	}
	return (long)n * factorial(n-1);
}

/*
 * Send message to MP2 module to register for rate-monotonic scheduling.
 */
void registerWithRMS()
{
	char buffer[100];
	sprintf(buffer, "echo R, %d, %ld, %ld > /proc/mp2/status", PID, PERIOD, COMPUTATION);
	system(buffer);
}

/*
 * Read through list of processes registered with MP2 module.
 * Return true if this process is listed else false.
 */
char isRegisteredWithRMS()
{
	FILE * file = fopen("/proc/mp2/status", "r");
	int pid;
	while(EOF != fscanf(file, "%d\n", &pid))
	{
		if(pid == PID)
		{
			fclose(file);
			return 1;
		}
	}
	fclose(file);
	return 0;
}

/*
 * Send yield message to MP2 module.
 */
void yieldRMS()
{
	char buffer[100];
	sprintf(buffer, "echo Y, %d > /proc/mp2/status", PID);
	system(buffer);
}

/*
 * Send message to MP2 module to deregister for rate-monotonic scheduling.
 */
void deregisterWithRMS()
{
	char buffer[100];
	sprintf(buffer, "echo D, %d > /proc/mp2/status", PID);
	system(buffer);
}

int main(int argc, char * args[])
{
	if(argc != 3)
	{
		printf("%s [# of jobs] [period]\n", args[0]);
		return 1;
	}
	JOBS = atoi(args[1]);
	PERIOD = atol(args[2]);
	PID = getpid();
	registerWithRMS();
	if(!isRegisteredWithRMS())
	{
		printf("[%d] Failed to register.\n", PID);
		return 2;
	}
	else
	{
		printf("[%d] Succesfully registered.\n", PID);
	}
	int x;
	yieldRMS();
	for(x = 0; x < JOBS; x++)
	{
		time_t rawtime;
		struct tm * timeinfo;
		time(&rawtime);
		timeinfo = localtime(&rawtime);
		printf("[%d] Work started at %s\n", PID, asctime(timeinfo));
		// START WORK UNIT
		int i, j;
		for(j = 0; j < 1000000; j++)
		{
			for(i = 0; i < 20; i++)
			{
				factorial(i);
			}
		}
		// END WORK UNIT
		time(&rawtime);
		timeinfo = localtime(&rawtime);
		printf("[%d] Work finished at %s\n", PID, asctime(timeinfo));
		yieldRMS();
	}
	deregisterWithRMS();
	if(isRegisteredWithRMS())
	{
		printf("[%d] Failed to deregister.\n", PID);
		return 3;
	}
	return 0;
}
